import { commandController } from "./controllers.js";
import { argsProccesor } from "./fs_utils.js";
// Todo init function.
init();
export default async function init() {
    const dbPath = `./DB.json`;
    const DBtemplate = {
        "notes": [],
        "currentId": 1
    };
    let args = await argsProccesor(dbPath, DBtemplate);
    commandController(args);
}
