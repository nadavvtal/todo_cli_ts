export interface Note {
    id: number,
    body: string,
    checked: boolean
}

export interface Params {
    [key: string]: string;
}

export interface DataBase {
    notes: Note[];
    currentId: number;
}
export type renderFunc = (DB: DataBase) => void;