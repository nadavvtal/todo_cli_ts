import log from "@ajar/marker";
import { DataBase } from "./types.js";


// Render list from database.
export function showList(DB: DataBase): void {
    DB.notes.forEach((element) => {
        if (element.checked) {
            log.green(`✓ ${element.id} ${element.body}`);
        } else {
            log.blue(`☐ ${element.id} ${element.body}`);
        }
    });
}

export function showHeader(){
    log.magenta("My to-do list:");
    log.magenta("Done | Id | Text");
}