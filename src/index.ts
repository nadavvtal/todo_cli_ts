import path from "path/posix";
import {commandController} from "./controllers.js"
import { argsProccesor } from "./fs_utils.js";
import {DataBase} from "./types.js"

// Todo init function.
init();
export default async function init(): Promise<void> {
        const dbPath: string = `./DB.json`; 
        const DBtemplate: DataBase = {
            "notes": [

            ],
            "currentId": 1
        }


        let args = await argsProccesor(dbPath,DBtemplate);
         
        commandController(args);
}








