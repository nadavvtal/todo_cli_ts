import fs from "fs/promises";
import log from "@ajar/marker";
import { DataBase } from "./types.js";


export async function argsProccesor(DBpath:string,DBtemplate:DataBase){

    // Get params from CLI and initialize arguments.
    const [command, ...args]: string[] = process.argv.slice(2);
    
    const encode: "utf-8" = 'utf-8';

    // Create DB.json file if there isn't such.
    try {
        const dbExist: void = await fs.access(DBpath);

    } catch (err) {
        try {
            await fs.writeFile(DBpath, "");
            log.green("New DB.json file created");

        } catch (err) {
            log.red(err);
            throw new Error("Erron in create new DB.json file");

        }
    }
    let DBjson: string = await fs.readFile(DBpath, encode);
    console.log(`For help use command: "help"`);

    // Checks if data base is empty.
    if (!DBjson) {
        writeToFile(DBtemplate, DBpath);
    }
    DBjson = await fs.readFile(DBpath, encode);
    let DB: DataBase = JSON.parse(DBjson);

    const fsParams ={
        DB,
        DBpath,
        args : [...args],
        command,
    }

    return fsParams;

}

export async function writeToFile(dataToWrite:DataBase, path: string): Promise<void> {

    try {
    await fs.writeFile(`./DB.json`,JSON.stringify(dataToWrite,null,2));
    } catch (error) {
        log.red("Error in writing to data base", error);
        throw new Error("Error");
    }

}