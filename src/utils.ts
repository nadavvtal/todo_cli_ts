import { DataBase,Note } from "./types.js";
import log from "@ajar/marker"

export function checkNoteInObjectArr(id: number, objArr:Note[]): Note[] {

    let found: boolean = false;
    objArr = objArr.map((note) => {
        if (note.id === Number(id)) {
            found = true;
            note.checked = !note.checked;
            return note;
        } else {
            return note;
        }
    });
    if (!found) {
        log.red("No such id");
    }

    return objArr;
}

// Delete Note from  Note array.
export function deleteteNoteFromArr(id: number, arr: Note[]):Note[] {

    let found: boolean = false;
    arr = arr.filter((item) => {
        if (item.id === Number(id)) {
            found = true;
        }
        return item.id !== Number(id);
    })
    found ? log.green(`Note id : ${id} deleted.`) : log.red(`No such id : ${id}`);

    return arr;
}

// Clear checked Notes from Notes array.
export function clearCheckedFromArr(NotesArr: Note[]): Note[] {

    NotesArr = NotesArr.filter((note) => {
        return note.checked === false;
    });

    return NotesArr;
}


// Add note element to array.
export function addNoteToArr(id: number, body: string[], arr:[Note]):[Note]{

    const newNote: Note = {
        "id": id,
        "body": body.join(" "),
        "checked": false
    }
    arr.push(newNote);
    return arr;
}
