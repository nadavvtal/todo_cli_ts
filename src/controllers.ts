import {Note,DataBase,renderFunc} from "./types.js";
import { showHelp} from "./scheme.js";
import { writeToFile } from "./fs_utils.js";
import { checkNoteInObjectArr ,deleteteNoteFromArr,clearCheckedFromArr,addNoteToArr} from "./utils.js";
import { showList,showHeader } from "./UI.js";
import log from "@ajar/marker";

export function commandController(params:any){

    let{DB,DBPath,command,args} = params;

    enum commands {
        ADD = "add",
        DELETE = "delete",
        SHOW = "show",
        FILTER = "filter",
        HELP = "help",
        CHECK = "check",
        RESET = "reset-noregrets",
        CLEAR = "clear-done",
    }

    // Main commands manager.
    switch (command) {
        case commands.ADD:
            DB.notes = addNoteToArr(DB.currentId, args, DB.notes);
            DB.currentId = increaseByOne(DB.currentId);
            writeToFile(DB, DBPath);
            showHeader();
            showList(DB);
            break;

        case commands.DELETE:
            const deleteNoteId = args[0];
            DB.notes = deleteteNoteFromArr(Number(deleteNoteId), DB.notes);
            writeToFile(DB, DBPath);
            showHeader();
            showList(DB);
            break;

        case commands.CHECK:
            let checkNoteId = args[0];
            DB.notes = checkNoteInObjectArr(Number(checkNoteId), DB.notes);
            writeToFile(DB, DBPath);
            showHeader();
            showList(DB);
            break;

        case commands.SHOW:
            showHeader();
            showList(DB);
            break;

        case commands.HELP:
            showHelp();
            break;

        case commands.FILTER:
            filterController(DB, args[0], showList);
            break;

        case commands.RESET:
            DB = resetDb(DB);
            writeToFile(DB, DBPath);
            break;

        case commands.CLEAR:
            DB.notes = clearCheckedFromArr(DB.notes);
            writeToFile(DB, DBPath);
            showHeader();
            showList(DB);
            break;

        default:
            log.red(`${command} is not legal command (try add,delete,check or help for more).`);
    }

}


// Filter manager function to handle filters rendering.
export function filterController(db: DataBase, filterFlag: string, showFunction: renderFunc): void {

    enum filters {
        ALL = "all",
        TODO = "todo",
        DONE = "done"
    }

    let show: DataBase = {
        notes: [],
        currentId: 0
    };

    switch (filterFlag) {
        case filters.ALL:
            showFunction(db);
            break;
        case filters.DONE:
            show.notes = db.notes.filter((note: Note) => {
                return note.checked === true;
            })
            showFunction(show);
            break;

        case filters.TODO:
            show.notes = db.notes.filter((note: Note) => {
                return note.checked === false;
            })
            showFunction(show);
            break;
        default:
            log.red(`${filterFlag} is not legal filter. (try done,todo,all).`);
    }

}


// Reset database and counter.
export function resetDb(db: DataBase): DataBase {
    db.notes = [];
    db.currentId = 1;
    log.red("Data base cleaned");
    return db;
}


// Increase number by one.
export function increaseByOne(id: number): number {
    return id+ 1;
}