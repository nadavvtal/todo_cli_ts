import log from "@ajar/marker";
// Render help instructions.
export function showHelp(): void {
    log.green("To-Do list commands:");
    console.log(`Add post: add "note example"`);
    console.log(`Delete note: delete "note id"`);
    console.log(`Check item: check "item id" `);
    console.log(`Show list: show`);
    console.log(`Filter notes : filter "filter flag" ("todo","done","all")`);
    console.log(`Remove completed tasks: clear-done`);
    log.red(`Reset data base: reset-noregrets`);
}



